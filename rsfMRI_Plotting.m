% rs-fMRI RS analysis
% HISTORY
%   2017-05-05 Foldes: Created
%   2017-10-19 Foldes: Updated
%   2017-10-26 Foldes: Updated
%   2017-10-30 Foldes: Updated
%   2018-05-31 Foldes: Rm rating

clc
clear

global PathStruct

data_path =     fullfile(PathStruct.base_git,  '\rs-hh\Data\');
RS =            h__rsfMRI_Prep(data_path);%  Creates RS structure

Outcomes =      load( fullfile(data_path,'Intervension_Outcomes.mat') );
Controls =      load( fullfile(data_path,'Control_Outcomes.mat') ); % make sure to add 7, 24, and 27 to this list

Laser_file =    fullfile(data_path,'Laser_voxel_SubjectScanVoxelXYZ.txt'); % to make: Read_Varinas_Voxel_Coordinates.m
% Subject Scan RS X Y Z
Laser =         load(Laser_file);


%% Process
plot_flag = 1;
FLAG_rating_color = false;
noisy_files_to_remove = [8 19 33 35]; % iscan


%  Color =      Cluster number
%  Outline =    RS was removed
%  Shape =      Rating (o,diamond,square)

scan_id_all =   [RS.scan_id];
scan_list =     unique(scan_id_all);
nscans =        length(scan_list);

if plot_flag    
    disp('Outline = RS was removed')
%     disp('Color = Cluster')
%     disp('Shape = Rating (diamond,square,triangle)')
    disp('Shape = Color (Red,Orange,Yellow)')
end

clear Results
for iscan = 1:nscans
    % Get current scan ID to look up
    c_scan =        scan_list(iscan);
    rs_voxel_idx =  find(scan_id_all == c_scan);
    
    % Info on this scan
    subject_num =   floor(c_scan/100);
    scan_num =      c_scan - 100 * floor(c_scan/100);
    
    % --- Laser ---
    laser_voxel_idx =   find( ismember( Laser(:,1),subject_num ) & ismember( Laser(:,2),scan_num ) );
    % laser coordinates
    laser_coor = Laser(laser_voxel_idx,[4,5,6]);
    nlaser_voxels = size(laser_coor,1);
    
    % Check for unique coords
    unq_laser_coor = unique(laser_coor,'rows');
    if nlaser_voxels ~= length(unq_laser_coor)
        disp([subject_num scan_num])
    end
    laser_coor =    unq_laser_coor;
    nlaser_voxels = size(laser_coor,1);
            
    % --- RS ---
    clear rs_coor rs_rating
    nrs_voxels = length(rs_voxel_idx);
    for ivox = 1:nrs_voxels
        entry_idx =         rs_voxel_idx(ivox);
        rs_coor(ivox,:) =   RS( entry_idx ).coor;
        rs_rating(ivox) =   RS( entry_idx ).rating;
    end
    
    rs_ablated_idx =        find( [RS( rs_voxel_idx).ablated] );
    nrs_ablated_voxels =    length(rs_ablated_idx);    

    % RS voxels ablated by rating
    for irate = 1:3
        rating_idx =                    find( rs_rating == irate );
        rs_rating_cnt(irate) =          length(rating_idx);
        rs_rating_ablated(irate) =      sum( [RS( rs_voxel_idx (rating_idx) ).ablated] );
        rs_rating_ablated_perc(irate) =   100 * rs_rating_ablated(irate) / length(rating_idx);        
    end
    
    % remove any ablated rs-voxels from the laser
    [C,laser_overlap_idx,rs_overlap_idx] = intersect(laser_coor,rs_coor(rs_ablated_idx,:),'rows');
    noverlap(iscan) = size(C,1); % number of overlapping voxels (NOT USED)
    % Remove rs-ablated from laser
    laser_not_rs_coor = laser_coor;
    laser_not_rs_coor(laser_overlap_idx,:) = [];
    nlaser_voxels_not_rs = size(laser_not_rs_coor,1);

    
    % Plot the Laser and RS data together
    if plot_flag
        marker_str = 'o';
        if FLAG_rating_color
            color_list = prism(6);
        else
            %color_list = [1 0 0; 1 0 0; 1 0 0]; % REMOVE RATING COLOR
            color_list = [0 1 0; 0 1 0; 0 1 0]; % REMOVE RATING COLOR
        end
        
        % PLOT
        h = figure;
        hold all

        % plot all Laser voxels
        plot3( laser_not_rs_coor(:,1),laser_not_rs_coor(:,2),laser_not_rs_coor(:,3),...
            'MarkerFaceColor','k','MarkerEdgeColor','none','Marker','o','MarkerSize',10,...
            'LineStyle','none','LineWidth',3)
        
        % Plot rs
        Plot_rsfMRI_for_HH(rs_coor,rs_voxel_idx,RS,color_list);
        title(['Subject ' num2str( subject_num ) '; SurgNum ' num2str( scan_num )])
        
        if any( ismember(noisy_files_to_remove, iscan) ) % bad, label as such
            title(['NOISY FILE Subject ' num2str( subject_num ) ' SurgNum ' num2str( scan_num )])

        end
        
        view(35, 15);
        axis vis3d
        %         legend('Removed','Not',...
        %             'Rating 1','Rating 2','Rating 3',...
        %             'Location','NW')
        
        % Movie/Video for 9 surg #1
        %Figure_Rotate(gca,'nrotations',1,'duration_S',10,'file_name','Rotate_09_1_pre53_post14.avi')
     
    end
    
    % Clusters
    rs_clusters =   spm_clusters(rs_coor');
    nrs_clus =      max(rs_clusters);
    clear rs_cluster_removed_perc
    for iclus = 1:nrs_clus
        % how much of cluster was ablated?
        cur_clust_idx = find( rs_clusters == iclus);
        % How many of this cluster# were ablated / total number of voxels in this cluster
        rs_cluster_removed_perc(iclus) = 100 * sum( ismember( cur_clust_idx , rs_ablated_idx ) ) / length( cur_clust_idx );
    end
    
    % ---Collect numbers for analysis---
    Results(iscan).subject_num =        subject_num;
    Results(iscan).surgery_num =        scan_num;

    Results(iscan).laser_cnt =          nlaser_voxels;
    Results(iscan).ablated_cnt =        nlaser_voxels_not_rs + nrs_ablated_voxels;
    Results(iscan).rs_cnt =             nrs_voxels;
    Results(iscan).rs_ablated_cnt =     nrs_ablated_voxels;
    
    % Demographic stuff
    demo_idx =  find( ismember( Outcomes.SubjectNumber,subject_num ) & ismember( Outcomes.surgerynumber,scan_num ) );
    Results(iscan).hh_cnt =             Outcomes.totalnoofcoordinators(demo_idx);
    Results(iscan).pre_seizure_rate =   Outcomes.seizurefreq_pre(demo_idx);
    Results(iscan).post_seizure_rate =  Outcomes.seizurefreq_post(demo_idx);
    Results(iscan).seizure_improv =     Results(iscan).post_seizure_rate - Results(iscan).pre_seizure_rate;
    Results(iscan).seizure_improv_perc =  -100 * Results(iscan).seizure_improv / Results(iscan).pre_seizure_rate;
    
    %     grade = cell2mat( Outcomes.seizure_improvement_grade(demo_idx) );
    %     if isnumeric(grade)
    %         Results(iscan).seizure_improvement_grade =  grade;
    %         Results(iscan).post_seizure_type =          'N';
    %     else
    %         % For cases w/o a number, add 1 (seizure free)
    %         if length(grade) == 1
    %             grade(2) = '1';
    %         end
    %
    %         switch grade(1)
    %             case {'A','B'}
    %                 Results(iscan).seizure_improvement_grade =	str2num( grade(2) );
    %                 Results(iscan).post_seizure_type =          grade(1);
    %             otherwise
    %                 Results(iscan).seizure_improvement_grade =	str2num( grade(1) );
    %                 Results(iscan).post_seizure_type =          grade(2);
    %         end
    %     end
    
    Results(iscan).hh_ablated_perc =    100 * Results(iscan).ablated_cnt / Results(iscan).hh_cnt;
    Results(iscan).rs_perc =            100 * Results(iscan).rs_cnt / Results(iscan).hh_cnt;
    Results(iscan).rs_ablated_perc =    100 * Results(iscan).rs_ablated_cnt / Results(iscan).rs_cnt;
    Results(iscan).rs_remaining =       Results(iscan).rs_cnt - Results(iscan).rs_ablated_cnt;
    Results(iscan).rs_remaining_perc =  100 * Results(iscan).rs_remaining / Results(iscan).rs_cnt;

    Results(iscan).rs_cnt_by_rating1 =          rs_rating_cnt(1);
    Results(iscan).rs_perc_by_rating1 =         100 * rs_rating_cnt(1) / Results(iscan).hh_cnt;
    Results(iscan).rs_ablated_by_rating1 =      rs_rating_ablated(1);
    Results(iscan).rs_ablated_by_rating1_perc = rs_rating_ablated_perc(1);
    Results(iscan).rs_cnt_by_rating2 =          rs_rating_cnt(2);
    Results(iscan).rs_perc_by_rating2 =         100 * rs_rating_cnt(2) / Results(iscan).hh_cnt;
    Results(iscan).rs_ablated_by_rating2 =      rs_rating_ablated(2);
    Results(iscan).rs_ablated_by_rating2_perc = rs_rating_ablated_perc(2);
    Results(iscan).rs_cnt_by_rating3 =          rs_rating_cnt(3);
    Results(iscan).rs_perc_by_rating3 =         100 * rs_rating_cnt(3) / Results(iscan).hh_cnt;
    Results(iscan).rs_ablated_by_rating3 =      rs_rating_ablated(3);
    Results(iscan).rs_ablated_by_rating3_perc = rs_rating_ablated_perc(3);
    
   
    %     Results(iscan).nrs_clus =           nrs_clus;
    %     Results(iscan).nrs_clus_rm =        sum(rs_cluster_removed_perc == 100);
    %     Results(iscan).nrs_clus_rm_perc =   100 * sum(rs_cluster_removed_perc == 100) / nrs_clus;
    %     Results(iscan).nrs_clus_left =      sum(rs_cluster_removed_perc ~= 100);
    %     Results(iscan).nrs_clus_left_perc = 100 * sum(rs_cluster_removed_perc ~= 100) / nrs_clus;
    %     Results(iscan).any_rs_clus_left =   sum( Results(iscan).nrs_clus_left > 0 );
    %
    %     Results(iscan).nrs_clus_untouched =         sum(rs_cluster_removed_perc == 0);
    %     Results(iscan).nrs_clus_untouched_perc =    100 * sum(rs_cluster_removed_perc == 0) / nrs_clus;
    %     Results(iscan).any_rs_clus_untouched =      sum( Results(iscan).nrs_clus_untouched > 0 );

end

% Get total number of surgeries
[total_surg,id] = unique_count([Results.subject_num]);
for i_id = 1:length(id)
    match_idx = find( [Results.subject_num] == id(i_id));
    for iscan = 1:length(match_idx)
        Results( match_idx(iscan)) .total_surg = total_surg(i_id);
    end
end

%% Remove Bad scans
%     7|1 1/15/2013 [8]
%     14|1 [19]
%     25|1 3/22/2013 [33]
%     27|1 4/8/2016 [35]

Results_Intervension = Results;

% noisy_files_to_remove = [8 19 33 35];
Results_Intervension(noisy_files_to_remove) = [];

% Write_Struct2TDF(Results_Intervension,fullfile(data_path,['Results_Intervension.txt']))

% Results_Controls_w_Laser = Results(noisy_files_to_remove);
% Write_Struct2TDF(Results_Controls_w_Laser,fullfile(data_path,['Results_Controls_w_Laser.txt']))

%% All, but less fields for writing out
% clear Results_w_Laser
% 
% for i = 1:length(Results)
%     Results_w_Laser(i).subject_num = Results(i).subject_num;
%     Results_w_Laser(i).surgery_num = Results(i).surgery_num;
%     Results_w_Laser(i).hh_cnt = Results(i).hh_cnt;
%     Results_w_Laser(i).hh_ablated_perc = Results(i).hh_ablated_perc;
%     Results_w_Laser(i).pre_seizure_rate = Results(i).pre_seizure_rate;
%     Results_w_Laser(i).post_seizure_rate = Results(i).post_seizure_rate;
%     Results_w_Laser(i).seizure_improv_perc = Results(i).seizure_improv_perc;
% end
% Write_Struct2TDF(Results_w_Laser,fullfile(data_path,['Results_w_Laser.txt']))

%% Control data

ncontrols = length( Controls.SubjectNumber );

clear Results_Control
for icontrol = 1:ncontrols
    Results_Control(icontrol).subject_num =             Controls.SubjectNumber(icontrol);
    Results_Control(icontrol).surgery_num =             Controls.surgerynumber(icontrol);    
    Results_Control(icontrol).hh_cnt =                  Controls.totalnoofcoordinators(icontrol);
    Results_Control(icontrol).pre_seizure_rate =        Controls.seizurefreq_pre(icontrol);
    Results_Control(icontrol).post_seizure_rate =       Controls.seizurefreq_post(icontrol);
    Results_Control(icontrol).seizure_improv =          Results_Control(icontrol).post_seizure_rate - Results_Control(icontrol).pre_seizure_rate;
    Results_Control(icontrol).seizure_improv_perc =     -100 * Results_Control(icontrol).seizure_improv / Results_Control(icontrol).pre_seizure_rate;
end

% Write_Struct2TDF(Results_Control,fullfile(data_path,['Results_Control.txt']))

%%


Plot_Bars({'RS not considered (n=15)','RS ablated (n=36)'},{[Results_Control.seizure_improv_perc],[Results_Intervension.seizure_improv_perc]}, ...
    'error_method','sem','IndPoints',0)
box on
ylim([-0 101])
ylabel('Seizure Improvement (%)')

[~,p] = ttest2([Results_Control.seizure_improv_perc],[Results_Intervension.seizure_improv_perc]);
% p = ranksum([Results_Control.seizure_improv_perc],[Results_Intervension.seizure_improv_perc]);
text2plot = sprintf('No-RS (n=%i) vs. RS (n=%i) \n p = %0.3f', length(Results_Control), length(Results_Intervension),p); 
title(text2plot)

(mean([Results_Intervension.seizure_improv_perc]) - mean([Results_Control.seizure_improv_perc]))


%% Ablation vs. improvement by Rating

regress_flag =  1;
pp_flag =       1;
annotate_flag = 1;

clear group_idx
group_idx{1} =  1:length( [Results_Intervension.surgery_num]);

% group_idx{1} =  find( [Results_Intervension.surgery_num] == 1 );
% group_idx{2} =  find( [Results_Intervension.surgery_num]>= 2 );
% % group_idx{3} =  find( [Results_Intervension.surgery_num] >= 3 );

clear group_idx_all_Results
group_idx_all_Results{1} =  1:length( [Results.surgery_num]);


h = figure;hold all

clear x_field y_field

x_field = 'rs_ablated_by_rating1_perc';
y_field = 'seizure_improv_perc';
subplot(1,4,1);hold all
h = h__Plot_RS_outcomes_pp(h,Results_Intervension,group_idx,x_field,y_field,'regress_flag',regress_flag,'pp_flag',pp_flag,'annotate_flag',annotate_flag);
xlabel('RS Ablated - Rating 1 (%)');ylabel('Seizure Improvment (%)')

x_field = 'rs_ablated_by_rating2_perc';
y_field = 'seizure_improv_perc';
subplot(1,4,2);hold all
h = h__Plot_RS_outcomes_pp(h,Results_Intervension,group_idx,x_field,y_field,'regress_flag',regress_flag,'pp_flag',pp_flag,'annotate_flag',annotate_flag);
xlabel('RS Ablated - Rating 2 (%)');ylabel('')

x_field = 'rs_ablated_by_rating3_perc';
y_field = 'seizure_improv_perc';
subplot(1,4,3);hold all
h = h__Plot_RS_outcomes_pp(h,Results_Intervension,group_idx,x_field,y_field,'regress_flag',regress_flag,'pp_flag',pp_flag,'annotate_flag',annotate_flag);
xlabel('RS Ablated - Rating 3 (%)');ylabel('')

x_field = 'hh_ablated_perc';
y_field = 'seizure_improv_perc';
subplot(1,4,4);hold all
h = h__Plot_RS_outcomes_pp(h,Results,group_idx_all_Results,x_field,y_field,'regress_flag',regress_flag,'pp_flag',0,'annotate_flag',annotate_flag);
xlim([-2 120]);ylim([-2 102])
xlabel('HH Region Ablated (%)');ylabel('')

Figure_Stretch(3)


%% Exploring

regress_flag =  1;
pp_flag =       0;
annotate_flag = 1;

clear group_idx
group_idx{1} =  1:length( [Results_Intervension.surgery_num]);

group_idx{1} =  find( [Results_Intervension.surgery_num] == 1 );
% group_idx{2} =  find( [Results_Intervension.surgery_num] >= 2 );
% % group_idx{3} =  find( [Results_Intervension.surgery_num] >= 3 );

% group_idx{1} =  find( [Results_Intervension.total_surg] == 1 );
% group_idx{2} =  find( [Results_Intervension.total_surg] >= 2 );

clear x_field y_field
cnt = 1;

x_field{cnt} =    'rs_cnt';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;

x_field{cnt} =    'rs_perc';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;

x_field{cnt} =    'rs_perc_by_rating1';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;

x_field{cnt} =    'rs_perc_by_rating2';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;

x_field{cnt} =    'rs_perc_by_rating3';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;

x_field{cnt} =    'hh_cnt';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;

nplots = length(x_field);

h = figure;hold all
for iplot = 1:nplots
    subplot(1,nplots,iplot);hold all
    h = h__Plot_RS_outcomes_pp(h,Results_Intervension,group_idx,x_field{iplot},y_field{iplot},'regress_flag',regress_flag,'pp_flag',pp_flag,'annotate_flag',annotate_flag);
end
% plot(get(gca,'XLim'),get(gca,'YLim'),'k--')

Figure_Stretch(nplots)

% xlabel('RS Target (voxels)');ylabel('Seizure Improvment (%)');ylim([-2 102]);


%% Exploring

regress_flag =  1;
pp_flag =       0;
annotate_flag = 0;

clear group_idx
group_idx{1} =  1:length( [Results_Intervension.surgery_num]);

% group_idx{1} =  find( [Results_Intervension.surgery_num] == 1 );
% group_idx{2} =  find( [Results_Intervension.surgery_num] >= 2 );
% % group_idx{3} =  find( [Results_Intervension.surgery_num] >= 3 );

clear x_field y_field
cnt = 1;

x_field{cnt} =    'rs_ablated_by_rating1_perc';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;

x_field{cnt} =    'hh_ablated_perc';
y_field{cnt} =    'post_seizure_rate';
cnt = cnt + 1;


nplots = length(x_field);

h = figure;hold all
for iplot = 1:nplots
    subplot(1,nplots,iplot);hold all
    h = h__Plot_RS_outcomes_pp(h,Results_Intervension,group_idx,x_field{iplot},y_field{iplot},'regress_flag',regress_flag,'pp_flag',pp_flag,'annotate_flag',annotate_flag);
end
% plot(get(gca,'XLim'),get(gca,'YLim'),'k--')

Figure_Stretch(2)

% xlabel('RS Target (voxels)');ylabel(' Seizure Improvment (%)');ylim([-2 102]);