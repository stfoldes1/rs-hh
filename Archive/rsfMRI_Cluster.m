% rs-fMRI Voxel analysis
% HISTORY
%   2017-05-05 Foldes: Created
%   2017-10-19 Foldes: Updated

% Make into Voxel objects

% data_path = 'D:\Data\rsfMRI-HH\'; %'D:\Data\rsfMRI\
% uiopen(fullfile(data_path,'pre-cluster.xlsx'),1)
% 
% 
HH_file =  'D:\Data\rsfMRI-HH\HH_voxel_SubjectScanVoxelXYZ.txt';
% Subject Scan Voxel X Y Z
HH = load(HH_file);

h__rsfMRI_Prep

%% Plot if removed or not
%  Color =      Cluster number
%  Outline =    Voxel was removed
%  Shape =      Rating (o,diamond,square)

nclust = 2;

scan_id_all =   [Voxel.scan_id];
scan_list =     unique(scan_id_all);
nscans =        length(scan_list);

marker_str = 'o';
color_list = hsv(8);

disp('Color = Cluster')
disp('Outline = Voxel was removed')
disp('Shape = Rating (o,diamond,square)')

for iscan = 21%:nscans
    c_scan =        scan_list(iscan);
    
    scan_id_list =  find(scan_id_all == c_scan);
    
    h = Plot_HH_and_rsfMRI(c_scan,scan_id_list,Voxel,HH,subject_num,scan_num,color_list,marker_str);
    
end
 


