% rs-fMRI RS analysis
% HISTORY
%   2017-05-05 Foldes: Created
%   2017-10-19 Foldes: Updated
%   2017-10-26 Foldes: Updated

clear

data_path = 'C:\Research\Code\Repos\rs-hh\Data\';
RS = h__rsfMRI_Prep(data_path);%  Creates RS structure

Demograph = load( fullfile(data_path,'HH_Demograph.mat') );

Laser_file =  'D:\Data\rsfMRI-HH\Laser_voxel_SubjectScanVoxelXYZ.txt'; % to make: Read_Varinas_Voxel_Coordinates.m
% Subject Scan RS X Y Z
Laser = load(Laser_file);

HH_file =  'D:\Data\rsfMRI-HH\HH_voxel_SubjectScanVoxelXYZ.txt'; % to make: Read_Varinas_Voxel_Coordinates.m
% Subject Scan RS X Y ZLaser = load(Laser_file);
HH = load(HH_file);


%% Process
plot_flag = 1;

%  Color =      Cluster number
%  Outline =    RS was removed
%  Shape =      Rating (o,diamond,square)

scan_id_all =   [RS.scan_id];
scan_list =     unique(scan_id_all);
nscans =        length(scan_list);

if plot_flag    
    disp('Color = Cluster')
    disp('Outline = RS was removed')
    disp('Shape = Rating (diamond,square,triangle)')
end

clear Results
for iscan = 1:nscans
    % Get current scan ID to look up
    c_scan =        scan_list(iscan);
    rs_voxel_idx =  find(scan_id_all == c_scan);
    
    % Info on this scan
    subject_num =   floor(c_scan/100);
    scan_num =      c_scan - 100 * floor(c_scan/100);
    
    % --- Laser ---
    laser_voxel_idx =   find( ismember( Laser(:,1),subject_num ) & ismember( Laser(:,2),scan_num ) );
    % laser coordinates
    laser_coor = Laser(laser_voxel_idx,[4,5,6]);
    nlaser_voxels = size(laser_coor,1);
    
    % Check for unique coords
    unq_laser_coor = unique(laser_coor,'rows');
    if nlaser_voxels ~= length(unq_laser_coor)
        disp([subject_num scan_num])
    end
    laser_coor =    unq_laser_coor;
    nlaser_voxels = size(laser_coor,1);
    
    % --- HH ---
    hh_voxel_idx =   find( ismember( HH(:,1),subject_num ) & ismember( HH(:,2),scan_num ) );
    % hh coordinates
    hh_coor = HH(hh_voxel_idx,[4,5,6]);
    nhh_voxels = size(hh_coor,1);
    
    % Check for unique coords
    unq_hh_coor = unique(hh_coor,'rows');
    if nhh_voxels ~= length(unq_hh_coor)
        disp([subject_num scan_num])
    end
    hh_coor =    unq_hh_coor;
    nhh_voxels = size(hh_coor,1);
        
    % --- RS ---
    clear rs_coor rs_rating
    nrs_voxels = length(rs_voxel_idx);
    for ivox = 1:nrs_voxels
        entry_idx =         rs_voxel_idx(ivox);
        rs_coor(ivox,:) =   RS( entry_idx ).coor;
        rs_rating(ivox) =   RS( entry_idx ).rating;
    end
    
    rs_ablated_idx =        find( [RS( rs_voxel_idx).ablated] );
    nrs_ablated_voxels =    length(rs_ablated_idx);    

    % RS voxels ablated by rating
    for irate = 1:3
        rating_idx =                    find( rs_rating == irate );
        rs_rating_ablated(irate) =      sum( [RS( rs_voxel_idx (rating_idx) ).ablated] );
        rs_rating_ablated_pp(irate) =   100 * rs_rating_ablated(irate) / length(rating_idx);        
    end
    
    % remove any ablated rs-voxels from the laser
    [C,laser_overlap_idx,rs_overlap_idx] = intersect(laser_coor,rs_coor(rs_ablated_idx,:),'rows');
    noverlap(iscan) = size(C,1); % number of overlapping voxels (NOT USED)
    % Remove rs-ablated from laser
    laser_not_rs_coor = laser_coor;
    laser_not_rs_coor(laser_overlap_idx,:) = [];

    
    % Plot the Laser and RS data together
    if plot_flag
        marker_str = 'o';
        color_list = hsv(8);
        
        % PLOT
        h = figure;
        hold all
        % plot all hh voxels
        plot3( hh_coor(:,1),hh_coor(:,2),hh_coor(:,3),...
            'MarkerFaceColor','m','MarkerEdgeColor','none','Marker','o','MarkerSize',10,...
            'LineStyle','none','LineWidth',1)
        
        % plot all Laser voxels
        plot3( laser_coor(:,1),laser_coor(:,2),laser_coor(:,3),...
            'MarkerFaceColor','none','MarkerEdgeColor','k','Marker','o','MarkerSize',10,...
            'LineStyle','none','LineWidth',3)
        
        % Plot rs
        Plot_rsfMRI_for_HH(rs_coor,rs_voxel_idx,RS,color_list,marker_str);
        title(['Subject: ' num2str( subject_num ) '; Surg #' num2str( scan_num )])
        view(35, 15);
    end
    
    % Clusters
    
    rs_clusters =   spm_clusters(rs_coor');
    nrs_clus =  max(rs_clusters);
    clear rs_cluster_removed_pp
    for iclus = 1:nrs_clus
        % how much of cluster was ablated?
        cur_clust_idx = find( rs_clusters == iclus);
        % How many of this cluster# were ablated / total number of voxels in this cluster
        rs_cluster_removed_pp(iclus) = 100 * sum( ismember( cur_clust_idx , rs_ablated_idx ) ) / length( cur_clust_idx );
    end
    
    % ---Collect numbers for analysis---
    Results(iscan).subject_num =        subject_num;
    Results(iscan).scan_num =           scan_num;

    Results(iscan).hh_cnt =             nhh_voxels;
    Results(iscan).ablated_cnt =        nlaser_voxels;
    Results(iscan).rs_cnt =             nrs_voxels;
    Results(iscan).rs_ablated_cnt =     nrs_ablated_voxels;
    
    % Demographic stuff
    demo_idx =  find( ismember( Demograph.subject_ID,subject_num ) & ismember( Demograph.surgery_num,scan_num ) );
    %Results(iscan).hh_cnt =             Demograph.totalnoofcoordinators(demo_idx);
    Results(iscan).pre_seizure_rate =   Demograph.seizurefreq_pre(demo_idx);
    Results(iscan).post_seizure_rate =  Demograph.seizurefreq_post(demo_idx);
    Results(iscan).seizure_improv =     Results(iscan).post_seizure_rate - Results(iscan).pre_seizure_rate;
    Results(iscan).seizure_improv_pp =  -100 * Results(iscan).seizure_improv / Results(iscan).pre_seizure_rate;
    
    grade = cell2mat( Demograph.seizure_improvement_grade(demo_idx) );
    if isnumeric(grade)
        Results(iscan).seizure_improvement_grade =  grade;
        Results(iscan).post_seizure_type =          'N';
    else
        % For cases w/o a number, add 1 (seizure free)
        if length(grade) == 1
            grade(2) = '1';
        end
        
        switch grade(1)
            case {'A','B'}
                Results(iscan).seizure_improvement_grade =	str2num( grade(2) );
                Results(iscan).post_seizure_type =          grade(1);
            otherwise
                Results(iscan).seizure_improvement_grade =	str2num( grade(1) );
                Results(iscan).post_seizure_type =          grade(2);
        end
    end
    
    Results(iscan).hh_ablated_pp =      100 * Results(iscan).ablated_cnt / Results(iscan).hh_cnt;
    Results(iscan).rs_ablated_pp =      100 * Results(iscan).rs_ablated_cnt / Results(iscan).rs_cnt;
    Results(iscan).rs_remaining =       Results(iscan).rs_cnt - Results(iscan).rs_ablated_cnt;
    Results(iscan).rs_remaining_pp =    100 * Results(iscan).rs_remaining / Results(iscan).rs_cnt;

    Results(iscan).rs_ablated_by_rating1 = rs_rating_ablated(1);
    Results(iscan).rs_ablated_by_rating1_pp = rs_rating_ablated_pp(1);
    Results(iscan).rs_ablated_by_rating2 = rs_rating_ablated(2);
    Results(iscan).rs_ablated_by_rating2_pp = rs_rating_ablated_pp(2);
    Results(iscan).rs_ablated_by_rating3 = rs_rating_ablated(3);
    Results(iscan).rs_ablated_by_rating3_pp = rs_rating_ablated_pp(3);
    
   
    Results(iscan).nrs_clus =           nrs_clus;
    Results(iscan).nrs_clus_rm =        sum(rs_cluster_removed_pp == 100);
    Results(iscan).nrs_clus_rm_pp =     100 * sum(rs_cluster_removed_pp == 100) / nrs_clus;
    Results(iscan).nrs_clus_left =      sum(rs_cluster_removed_pp ~= 100);
    Results(iscan).nrs_clus_left_pp =   100 * sum(rs_cluster_removed_pp ~= 100) / nrs_clus;
    Results(iscan).any_rs_clus_left =  sum( Results(iscan).nrs_clus_left > 0 );
    
    Results(iscan).nrs_clus_untouched =     sum(rs_cluster_removed_pp == 0);
    Results(iscan).nrs_clus_untouched_pp =  100 * sum(rs_cluster_removed_pp == 0) / nrs_clus;
    Results(iscan).any_rs_clus_untouched =  sum( Results(iscan).nrs_clus_untouched > 0 );

end

% Get total number of surgeries
[total_surg,id] = unique_count([Results.subject_num]);
for i_id = 1:length(id)
    match_idx = find( [Results.subject_num] == id(i_id));
    for iscan = 1:length(match_idx)
        Results( match_idx(iscan)) .total_surg = total_surg(i_id);
    end
end

%% Remove Bad scans
%     7|1 1/15/2013 [8]
%     25|1 3/22/2013 [33]
%     27|1 4/8/2016 [35]

noise_files_to_remove = [8 33 35];
Results(noise_files_to_remove) = [];


%%
regress_flag =  0;
pp_flag =       1;

clear group_idx
group_idx{1} =  1:length( [Results.scan_num]);


% group_idx{1} =  find( [Results.scan_num] == 1 );
% group_idx{2} =  find( [Results.scan_num]>= 2 );
% % group_idx{3} =  find( [Results.scan_num] >= 3 );

h = figure;hold all

x_field = 'rs_ablated_by_rating1_pp';
y_field = 'seizure_improv_pp';

subplot(1,4,1);hold all
h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'regress_flag',regress_flag,'pp_flag',pp_flag);


x_field = 'rs_ablated_by_rating2_pp';
y_field = 'seizure_improv_pp';

subplot(1,4,2);hold all
h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'regress_flag',regress_flag,'pp_flag',pp_flag);


x_field = 'rs_ablated_by_rating3_pp';
y_field = 'seizure_improv_pp';

subplot(1,4,3);hold all
h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'regress_flag',regress_flag,'pp_flag',pp_flag);

x_field = 'hh_ablated_pp';
y_field = 'seizure_improv_pp';

subplot(1,4,4);hold all
h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'regress_flag',regress_flag,'pp_flag',pp_flag);


Figure_Stretch(3)


%% 
clear group_idx
group_idx{1} =  1:length( [Results.scan_num]);

% group_idx{1} =  find( [Results.scan_num] == 1 );
% group_idx{2} =  find( [Results.scan_num]>= 2 );
% group_idx{3} =  find( [Results.scan_num] >= 3 );

h = figure;hold all

x_field = 'rs_ablated_pp';
y_field = 'seizure_improv_pp';

subplot(1,2,1);hold all
h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'regress_flag',1);
ylim([-5 110])

x_field = 'hh_ablated_pp';
y_field = 'seizure_improv_pp';

subplot(1,2,2);hold all
h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'regress_flag',1);
xlim([-5 110]); ylim([-5 110])

Figure_Stretch(2)

% plot(get(gca,'XLim'),get(gca,'YLim'),'k--')

%%
% 
% %% Surgery number
% 
% clear group_idx
% group_idx{1} =  find( [Results.scan_num] == 1 );
% group_idx{2} =  find( [Results.scan_num] == 2 );
% group_idx{3} =  find( [Results.scan_num] >= 3 );
% 
% h = figure;
% 
% x_field = 'hh_ablated_pp';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,1);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',1);
% ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)');
% 
% x_field = 'rs_ablated_pp';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,2);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',1);
% ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (%)');
% 
% Figure_Stretch(2)
% 
% 
% %% Number of surgeries, total
% 
% clear group_idx
% group_idx{1} =  find( [Results.total_surg] == 1 );
% group_idx{2} =  find( [Results.total_surg] == 2 );
% group_idx{3} =  find( [Results.total_surg] >= 3 );
% 
% h = figure;
% 
% x_field = 'hh_ablated_pp';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,1);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',1);
% ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)');
% 
% x_field = 'rs_ablated_pp';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,2);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',1);
% ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (%)');
% 
% Figure_Stretch(2)
% 
% 
% 
% %% Surgery number
% 
% clear group_idx
% group_idx{1} =  find( [Results.scan_num] == 1 );
% group_idx{2} =  find( [Results.scan_num] == 2 );
% group_idx{3} =  find( [Results.scan_num] >= 3 );
% 
% h = figure;
% 
% x_field = 'hh_cnt';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,1);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',0);
% ylabel('Seizure improvement (%)'); xlabel('Amount of HH (voxels)');
% 
% x_field = 'rs_ablated_cnt';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,2);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',0);
% ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (voxels)');
% 
% Figure_Stretch(2)
% 
% 
% %% Surgery number
% clear group_idx
% group_idx{1} =  find( [Results.scan_num] == 1 );
% % group_idx{2} =  find( [Results.scan_num] == 2 );
% % group_idx{3} =  find( [Results.scan_num] >= 3 );
% 
% h = figure;
% 
% x_field = 'hh_cnt';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,1);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',0,'regress_flag',1);
% ylabel('Seizure improvement (%)'); xlabel('Amount of HH (voxels)');
% 
% x_field = 'rs_ablated_cnt';
% y_field = 'seizure_improv_pp';
% 
% subplot(1,2,2);hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',0,'regress_flag',1);
% ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (voxels)');
% 
% Figure_Stretch(2)
% 
% %% PLOT
% 
% clear group_idx
% group_idx{1} =  find( [Results.seizure_improv_pp] < 50 );
% group_idx{2} =  find( [Results.seizure_improv_pp] >= 50 );
% 
% h = figure;
% 
% x_field = 'ablated_cnt';
% y_field = 'rs_ablated_cnt';
% 
% hold all
% h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,'pp_flag',0,'regress_flag',0);
% 
% ylabel('Amount ablated (voxels)'); xlabel('Amount of RS ablated (voxels)');
% 
% 
% 
% %%
% % figure
% % plot( 100*[Results.hh_ablated_pp],-100*[Results.seizure_improv_pp],'.k','MarkerSize',40)
% % ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)')
% %
% % plot( 100*[Results.rs_ablated_pp],-100*[Results.seizure_improv_pp],'.k','MarkerSize',40)
% % ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)')
% 
% % %%
% % figure; hold all
% % 
% % subplot(1,2,1);hold all
% % x2plot = 100*[Results.hh_ablated_pp];
% % y2plot = -100*[Results.seizure_improv_pp];
% % 
% % plot( x2plot,y2plot,'.k','MarkerSize',40)
% % xlim([-5 105]); ylim([-1 101])
% % Plot_Regression_Line(x2plot, y2plot)
% % ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)'); box on
% % 
% % 
% % subplot(1,2,2);hold all
% % x2plot = 100*[Results.rs_ablated_pp];
% % y2plot = -100*[Results.seizure_improv_pp];
% % 
% % plot( x2plot,y2plot,'.k','MarkerSize',40)
% % xlim([-5 105]); ylim([-1 101])
% % Plot_Regression_Line(x2plot, y2plot)
% % ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (%)'); box on
% % 
% % Figure_Stretch(2)
% % 
% % 
% % %%
% % h = figure; hold all
% % 
% % subplot(1,2,1);hold all
% % Plot_Scatter_wStruct(Results,'hh_ablated_pp','seizure_improv_pp','seizure_improvement_grade_str','annotate_flag',0,'fig',h);
% % ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)'); box on
% % yaxis_reverse
% % 
% % subplot(1,2,2);hold all
% % Plot_Scatter_wStruct(Results,'rs_ablated_pp','seizure_improv_pp','seizure_improvement_grade_str','annotate_flag',0,'fig',h);
% % ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (%)'); box on
% % yaxis_reverse
% % 
% % Figure_Stretch(2)
% % 
% % %%
% % h = figure; hold all
% % 
% % subplot(1,2,1);hold all
% % Plot_Scatter_wStruct(Results,'hh_ablated_pp','seizure_improv_pp','subject_num','annotate_flag',0,'fig',h);
% % ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)'); box on
% % yaxis_reverse
% % 
% % subplot(1,2,2);hold all
% % Plot_Scatter_wStruct(Results,'rs_ablated_pp','seizure_improv_pp','subject_num','annotate_flag',0,'fig',h);
% % ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (%)'); box on
% % yaxis_reverse
% % 
% % Figure_Stretch(2)
% % 
% % %%
% % h = figure; hold all
% % 
% % subplot(1,2,1);hold all
% % Plot_Scatter_wStruct(Results,'hh_ablated_pp','seizure_improv_pp','nrs_cluster','annotate_flag',0,'fig',h);
% % ylabel('Seizure improvement (%)'); xlabel('Amount of HH ablated (%)'); box on
% % yaxis_reverse
% % 
% % subplot(1,2,2);hold all
% % Plot_Scatter_wStruct(Results,'rs_ablated_pp','seizure_improv_pp','nrs_cluster','annotate_flag',0,'fig',h);
% % ylabel('Seizure improvement (%)'); xlabel('Amount of RS ablated (%)'); box on
% % yaxis_reverse
% % 
% % Figure_Stretch(2)
% % 
% % 
% 
