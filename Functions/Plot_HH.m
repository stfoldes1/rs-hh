% HISTORY
%   2017-08 Foldes: Created

function h = Plot_HH(h,HH,HH_voxel_idx)

    plot3( HH(HH_voxel_idx,4),HH(HH_voxel_idx,5),HH(HH_voxel_idx,6),...
        'MarkerFaceColor','k','MarkerEdgeColor','k','Marker','o','MarkerSize',10,...
        'LineStyle','none','LineWidth',1)
    %text( HH(found_idx,4)+.5,HH(found_idx,5)+.5,HH(found_idx,6), num2str(HH(found_idx,3)) );

end
