% Get Voxel coordinates from Varina's crazy files
%   2017-08-24 Foldes
%   2017-10-30 Foldes: Updated

base_path = 'D:\OneDrive - Phoenix Children''s Hospital\Projects\rs-fMRI\Data\Foldes-laser coordinates\';
out_file =  'C:\Research\Code\Repositories\rs-hh\Data\Laser_voxel_SubjectScanVoxelXYZ.txt';

% base_path = 'D:\Data\rsfMRI-HH\Foldes-laser coordinates\';
% out_file =  'D:\Data\rsfMRI-HH\Laser_voxel_SubjectScanVoxelXYZ.txt';
% base_path = 'D:\Data\rsfMRI-HH\Coordinates-wholeHH-subject\';
% out_file =  'D:\Data\rsfMRI-HH\HH_voxel_SubjectScanVoxelXYZ.txt';

% Get file list
file_names =    dir_file_names(base_path);
nfiles =        length(file_names);

fout_id = fopen( out_file ,'w');
%fprintf(fout_id,'Subject\tScan\tVoxel\tX\tY\tZ\n');



for ifile = 1:nfiles
    
    txt_file = file_names{ifile};
    
    % Pull subject name, scan #
    subject_num =   strfind_number( lower(txt_file) ,'subject-');
    scan_num =  	strfind_number( lower(txt_file) ,'scan-');
    
    %     fprintf(fout_id,'%s\n',txt_file);
    
    % Open file
    fid = fopen( fullfile(base_path, txt_file) ,'r');
    if fid==-1
        error(['Could not read file: ' txt_file])
        return
    end
    whole_file_str = char(fread(fid));
    fclose(fid);
    
    
    % Pull voxel coordinates
    
    voxel_num = 0;
    while length(whole_file_str) > 1
        [current_line, whole_file_str] = Get_Text_Line(whole_file_str);
        current_line =      replace(current_line,0); % 0 is a space for these file types
        current_line =      replace(current_line,'�');
        current_line =      replace(current_line,'�');
        
        underscore_idx =    strfind(current_line,'_'); % underscores are the key
        
        
        
        if length(underscore_idx) == 5
            % *_*_X_Y_Z_*.jpg (any number or values)
            
            voxel_num = voxel_num + 1;
            
            %   X = betwen 2nd and 3rd underscore
            X = str2num( current_line( underscore_idx(2)+1:underscore_idx(3)-1 ) );
            
            %   Y = betwen 3rd and 4th underscore
            Y = str2num( current_line( underscore_idx(3)+1:underscore_idx(4)-1 ) );
            
            %   Z = betwen 4th and 5th underscore
            Z = str2num( current_line( underscore_idx(4)+1:underscore_idx(5)-1 ) );
            
            fprintf(fout_id,'%i\t%i\t%i\t%i\t%i\t%i\n',subject_num,scan_num,voxel_num,X,Y,Z);
            
        elseif length(underscore_idx) == 4
            % X_Y_Z_*.jpg (any number or values)
            
            voxel_num = voxel_num + 1;
            
            %   X = betwen 2nd and 3rd underscore
            X = str2num( current_line( 1:underscore_idx(1)-1 ) );
            
            %   Y = betwen 3rd and 4th underscore
            Y = str2num( current_line( underscore_idx(1)+1:underscore_idx(2)-1 ) );
            
            %   Z = betwen 4th and 5th underscore
            Z = str2num( current_line( underscore_idx(2)+1:underscore_idx(3)-1 ) );
            
            fprintf(fout_id,'%i\t%i\t%i\t%i\t%i\t%i\n',subject_num,scan_num,voxel_num,X,Y,Z);
            
        else
            %             fprintf(fout_id,'\txxxxxx %s\n',current_line);
            
        end % is a valid string
    end % voxels
    %     fprintf(fout_id,'\n');
    %     pause(2)
end % file

fclose(fout_id);