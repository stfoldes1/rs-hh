%% 2017-10-23


function h = h__Plot_RS_outcomes_pp(h,Results,group_idx,x_field,y_field,varargin)


parms.annotate_flag =   1;
parms.pp_flag =         0;
parms.regress_flag =    0;
parms = varargin_extraction(parms,varargin);


ngroups = length(group_idx);

color_list = lines(ngroups);

figure(h); hold all


for igroup = 1:ngroups
    
    x2plot = [Results(group_idx{igroup}).(x_field)];
    y2plot = [Results(group_idx{igroup}).(y_field)];
    
    plot( x2plot,y2plot,'.','Color',color_list(igroup,:),'MarkerSize',30)
    
    if parms.pp_flag
        xlim([-2 102]); ylim([-2 102])
    end
    
    if parms.regress_flag
        if length(x2plot) > 1
            Plot_Regression_Line(x2plot, y2plot,'current_color',color_list(igroup,:),...
                'annotate_flag',parms.annotate_flag);
        end
    end
    
    axis square
    box on
    % Plot_Regression_Line(x2plot, y2plot)
    c_group = group_idx{igroup};
    if parms.annotate_flag
        for idot = 1:length(c_group)
            text(x2plot(idot),y2plot(idot), [num2str( Results(c_group(idot)).subject_num ) '|' num2str( Results(c_group(idot)).surgery_num )], ...
                'HorizontalAlignment','center',...
                'VerticalAlignment','Bottom',...
                'FontSize',10);
        end
    end
    ylabel(str4plot(y_field)); xlabel(str4plot(x_field));

end % group


