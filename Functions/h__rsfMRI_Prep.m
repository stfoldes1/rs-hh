% HISTORY
%   2017-05-05 Foldes: Created
%   2017-10-30 Foldes: Updated for new file

function RS = h__rsfMRI_Prep(data_path)

% uiopen(fullfile(data_path,'pre-cluster.xlsx'),1)
% load( fullfile(data_path,'pre-cluster.mat') );
% uiopen('C:\Users\SF\OneDrive - Phoenix Children''s Hospital\Projects\rs-fMRI\Data\Boerwinkle-HH-rs-DataAggregate-deident-10-30-17_CLEANEDSTF.xlsx',1)
load( fullfile(data_path,'rs_voxels_data.mat') );

%% Prepare data
nentries = length(Coordinate);

clear RS
for ientry = 1:nentries
    RS(ientry).subject_ID =     subjectID(ientry);
    RS(ientry).surgery_num =    surgeryno(ientry);
    RS(ientry).coor_num =       CoordinateNumber(ientry);
    RS(ientry).rating =         Rating(ientry);
    
    
    RS(ientry).overlap_FFE =    Scan1FFE(ientry);
    RS(ientry).overlap_DWI =    Scan2DWI(ientry);
    RS(ientry).overlap_remote = Scan3Remote(ientry);
    
    RS(ientry).ablated =        max([Scan1FFE(ientry), Scan2DWI(ientry), Scan3Remote(ientry)]);

    coor_str =                  Coordinate{ientry};
    
    % remove strange chars
    
    for ichar = 1:length(coor_str)
        if strcmp( coor_str(ichar),'_' ) | strfind( coor_str(ichar),'' )
            coor_str(ichar) = ' ';
        end
    end
    coor = str2num(coor_str);
    
    
    RS(ientry).coor(1) =       coor(1);
    RS(ientry).coor(2) =       coor(2);
    RS(ientry).coor(3) =       coor(3);
    
end

% clearvars -except RS HH

%% Unique voxel id

nvoxels = length(RS);

clear scan_id_all
for ivox = 1:nvoxels
    RS(ivox).scan_id =   RS(ivox).subject_ID * 100 + RS(ivox).surgery_num;
    % RS(ivox).scan_id =   [num2str(RS(ivox).subject_ID) '_' num2str(RS(ivox).surgery_num)];
    %scan_id_all{ivox} =     RS(ivox).scan_id;
end