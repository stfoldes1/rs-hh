% 2017-10-19 Foldes
% 2017-10-31 Foldes: Removed cluster
function Plot_rsfMRI_for_HH(rs_coor,scan_id_list,RS,color_list)


nvoxels = size(rs_coor,1);

for ivox = 1:nvoxels
    entry_idx = scan_id_list(ivox);
    
    
    switch RS( entry_idx ).rating
        case 1
            color_str =	color_list(1,:);
        case 2
            color_str = color_list(2,:);
        case 3
            color_str = color_list(3,:);
    end
    
    switch RS( entry_idx ).ablated
        case 1 % was removed
            edge_color =    'k';
            size_str =      10;
        case 0 % was not removed
            edge_color =    'none';
            size_str =      10;
    end
    
    
    plot3(rs_coor(ivox,1),rs_coor(ivox,2),rs_coor(ivox,3),...
        'MarkerFaceColor',color_str,'MarkerEdgeColor',edge_color,'Marker','o','MarkerSize',size_str,...
        'LineStyle','none','LineWidth',3)
end

%legend('Removed','Not',...
%    'Location','NW')

axis off
axis image
% axis vis3d