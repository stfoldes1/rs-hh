% 2017-10-19 Foldes
function Plot_rsfMRI_for_HH(rs_coor,scan_id_list,RS,color_list,marker_str)


nvoxels = size(rs_coor,1);

% clear rs_coor
% for ivox = 1:nvoxels
%     entry_idx =         scan_id_list(ivox);
%     rs_coor(ivox,:) = RS( entry_idx ).coor;
% end

cluster_id = spm_clusters(rs_coor');

for ivox = 1:nvoxels
    entry_idx = scan_id_list(ivox);
    
    color_str = color_list( cluster_id(ivox),: );
    
    
    
    % [idx,C] = kmeans(rs_coor,nclust);
    
    
    switch RS( entry_idx ).rating
        case 1
            marker_str =	'd';
        case 2
            marker_str =  	's';
        case 3
            marker_str = 	'^';
    end
    
    switch RS( entry_idx ).ablated
        case 1 % was removed
            edge_color =    'k';
            size_str =      15;
        case 0 % was not removed
            edge_color =    color_str;
            size_str =      10;
    end
    
    
    plot3(rs_coor(ivox,1),rs_coor(ivox,2),rs_coor(ivox,3),...
        'MarkerFaceColor',color_str,'MarkerEdgeColor',edge_color,'Marker',marker_str,'MarkerSize',size_str,...
        'LineStyle','none','LineWidth',3)
end

%legend('Removed','Not',...
%    'Location','NW')

axis off